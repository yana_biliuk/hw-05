// task 1

function getSum(a, b) {
    if (b===0){
        return 'Не ділемо на нуль'
    }else{return a / b;}

}
console.log (getSum(10,5));

// task 2

function getNumber(value) {
  let input;
  do {
    input = prompt(value);
  } while (isNaN(input));

  return parseFloat(input);
}

function getOperator() {
  let input;
  do {
    input = prompt("Ввеіть математичну операцію:");
  } while (!["+", "-", "*", "/"].includes(input));

  return input;
}

function getResult(number1, number2, input) {
  switch (input) {
    case "+":
      return number1 + number2;
    case "-":
      return number1 - number2;
    case "*":
      return number1 * number2;
    case "/":
      if (number2 !== 0) {
        return number1 / number2;
      } else {
        return "Ділення на нуль неможливе";
      }
    default:
      return "Такої операції не існує";
  }
}
number1 = getNumber("Введіть перше число:");
number2 = getNumber("Введіть друге число:");
mathOperator = getOperator();

let result = getResult(number1, number2, mathOperator);
alert(`Ваш результат ${result}` )

console.log(number1);
console.log(number2);
console.log(mathOperator);

console.log(`Результат виконання функції: ${result}`);

// task 3

function getNumber() {
  let input;
  do {
    input = prompt("Введіть число:");
  } while (isNaN(input));

  return parseFloat(input);
}

let number = getNumber();
console.log(number);


const getResult = (n) => {
  if (n === 0 || n === 1) {
    return 1;
  } else {
    let result = 1;
    for (let i = 2; i <= n; i++) {
      result *= i;
    }
    return result;
  }
};
const resultFact = getResult(number);

console.log(`Факторіал числа ${number} дорівнює ${resultFact}`)